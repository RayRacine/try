#lang scribble/manual
@require[@for-label[try
                    racket/base]]

@title{Try}
@author[(author+email "Raymond Racine" "ray.racine@gmail.com")]

@section{Try Library}

@defmodule[try]

@defstruct[Try-Failure ([exception exn?])]{
@racket[(struct: (T) Try-Failure ([exception : exn]))]

A computation that has failed with an exception.
}

@defstruct[Try-Success ([result T])]{
@racket[(struct: (T) Try-Success ([result : T]))]

A computation that has successfully returned some value.
}

@defthing[#:kind "Type" Try (U (Try-Failure exn?) (Try-Success T))] {
@racket[(define-type (Try T) (U (Try-Failure T) (Try-Success T)))]

The result of a computation that either successfully returned some value or has failed with a thrown exception.
}

@defthing[#:kind "Procedure" try-failure? (All (T) (Try T) -> Boolean)]{
Is the value of a Try type a Try-Failure.  The computation failed with
an exception.
}

@defthing[#:kind "Procedure" try-success? (All (T) (Try T) -> Boolean)]{
Is the value of a Try type a Try-Success. The computuation successfully
completed returning a result value.
}

@defthing[#:kind "Procedure" try-get (All (T) (Try T) -> T)]{
Gets the value of a Try if the Try succeeded otherwise throws the Try's failure exception.
}

@defthing[#:kind "Procedure" try-map (All (T U) (Try T) (T -> U) -> (Try U))]{
Apply the provided function to a Try's success value otherwise do nothing.
If the application fails the exception is the new value of the Try.
}

@defthing[#:kind "Procedure" try-flatmap (All (T U) (Try T) (T -> (Try U)) -> (Try U))]{
Apply the function to the value of a successful try, otherwise do nothing.
}

@defthing[#:kind "Procedure" try-or-else (All (T U) (Try T) (T -> U) (exn -> U) -> (Try U))]{
Apply the appropriate function to the Try.  The first function if the Try was successful.
The second function of the Try failed.  Always be one or the other of the two functions is applied.
}

@defthing[#:kind "Procedure" try-filter (All (T) (Try T) (T -> Boolean) -> (Try T))]{
If the Try's success value matches the provided predicate function simply return the Try.
If the Try's success value fails the predicate return a Try-Failure with an exception indicating
the predicate did not match.
If the Try was already failed then simply return the Try.
}

@defthing[#:kind "Procedure" try-exists? (All (T) (Try T) (T -> Boolean) -> Boolean)]{
Does the value of a successful Try match the provided predicate?
A failed Try never matches the predicate.
}

@defthing[#:kind "Procedure" try-invert (All (T) (Try T) -> (Try exn))]{
If the Try was successful return a failed try with a synthetic exception of exn:fail.
If the Try was failed then return the Try as a success where the result value is the failed exception.
}

@defthing[#:kind "Procedure" try-continue (All (T V W) (Try T) (T -> (Try V)) (exn -> (Try W)) -> (Try (U V W)))]{
Apply either the provided on-success procedure or the on-failure procedure to the given Try.
The value returned is of the union type of the return type of provided two procedures.
One procedure, either the success procedure or failure procedure is always applied.
}


@defthing[#:kind "Procedure" try-rescue (All (T) (Try T) ((Try-Failure T) -> (Option (Try T))) -> (Try T))]{
Rescue a failed computation if the rescue function is capable of doing so.
If the provided Try is a successful Try value then simply return this Try.
Otherwise we have a failed Try:
If the rescue procedure does return a success Try value for the given failed Try then return that success.
If the rescure procedure does not return a success Try value for the given failed Try then simply return the original failed Try.
}

@defthing[#:kind "Procedure" try-recover (All (T) (Try T) ((Try-Failure T) -> (Option T)) -> (Try T))]{
Similar to the @racket[try-rescue] procedure except the difference in the type signature of the recove/rescue procedure.
}

@defthing[#:kind "Procedure" try (All (T) (-> T) -> (Try T))]{
Evaluate the given closure in a Try that captures the closure successfully returning a value or failing with some exception.
The closure's success return value is returned as a successful Try value.
The closure's thrown exception is caught and returned as a failed Try value.
}

@defthing[#:kind "Syntax" with-try block...] {
Evaluate a block of statements/expression in a Try context catching any exceptions or returning as successful the value of the last expression.
}

@racketblock[
  (with-try
    (do-this)
    (do-that)
    (return-with-the-other))
]

@defthing[#:kind "Procedure" try->option (All (T) (Try T) -> (Option T))]{
Converts a Try to an Option.  A successful Try value becomes an Option value.
A failed Try value simply becomes a @racket[#f] Option value.
}

@section{Examples}

Note that Typed Racket's inference may need some hinting.

@racketblock[
(: scale-and-bump (-> Integer Integer Exact-Rational))
(define (scale-and-bump scale incr)
  (try-get ((inst try-map Exact-Rational Exact-Rational)
            (try-recover (with-try (/ 100 scale)) (λ (ex) 0))
            (λ ((x : Exact-Rational)) (+ x incr)))))

(scale-and-bump 2 1)
- : Exact-Rational
51

(scale-and-bump 0  1)
- : Exact-Rational
1
]